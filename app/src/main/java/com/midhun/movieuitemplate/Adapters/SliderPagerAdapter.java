package com.midhun.movieuitemplate.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.midhun.movieuitemplate.Models.Slide;
import com.midhun.movieuitemplate.R;

import java.util.List;

public class SliderPagerAdapter extends PagerAdapter {
    private Context context;
    private List<Slide> mList;

    public SliderPagerAdapter(Context context, List<Slide> mList) {
        this.context = context;
        this.mList = mList;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View slideLayout = inflater.inflate(R.layout.slide_item,null);
        ImageView slideImage = slideLayout.findViewById(R.id.slide_image);
        TextView slideText = slideLayout.findViewById(R.id.slide_title);
        slideImage.setImageResource(mList.get(position).getImage());
        slideText.setText(mList.get(position).getTitle());
        container.addView(slideLayout);
        return  slideLayout;

    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);

    }
}

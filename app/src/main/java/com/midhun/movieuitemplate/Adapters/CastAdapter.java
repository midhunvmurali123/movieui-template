package com.midhun.movieuitemplate.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.midhun.movieuitemplate.Models.Cast;
import com.midhun.movieuitemplate.R;

import java.util.List;

public class CastAdapter extends RecyclerView.Adapter<CastAdapter.CastViewHoldre> {
    Context context;
    List<Cast> castList;

    public CastAdapter(Context context, List<Cast> castList) {
        this.context = context;
        this.castList = castList;
    }

    @NonNull
    @Override
    public CastViewHoldre onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_cast,viewGroup,false);
        return new CastViewHoldre(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CastViewHoldre castViewHoldre, int i) {
        Glide.with(context).load(castList.get(i).getImgLink()).into(castViewHoldre.imageView);
        castViewHoldre.textView.setText(castList.get(i).getName());

    }

    @Override
    public int getItemCount() {
        return castList.size();
    }

    public class CastViewHoldre extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;

        public CastViewHoldre(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.img_cast);
            textView = itemView.findViewById(R.id.cast_name);
        }
    }
}
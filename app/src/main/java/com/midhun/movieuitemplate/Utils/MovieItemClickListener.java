package com.midhun.movieuitemplate.Utils;

import android.widget.ImageView;

import com.midhun.movieuitemplate.Models.Movies;

public interface MovieItemClickListener {
    void onMovieClick(Movies movies, ImageView movieImageView); //need imageview to make the shared animation between two activity
}

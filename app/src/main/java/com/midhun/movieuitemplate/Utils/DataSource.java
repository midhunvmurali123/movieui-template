package com.midhun.movieuitemplate.Utils;

import com.midhun.movieuitemplate.Models.Cast;
import com.midhun.movieuitemplate.Models.Movies;
import com.midhun.movieuitemplate.R;

import java.util.ArrayList;
import java.util.List;

public class DataSource {
    public static List<Movies> getPopularMovies(){
        List<Movies> moviesList = new ArrayList<>();
        moviesList.add(new Movies("Avengers Endgame", R.drawable.avengers_endgame_poster,R.drawable.avengers_endgame_bg,R.string.avengers_endgame));
        moviesList.add(new Movies("Inception",R.drawable.inception_poster,R.drawable.inception_bg,R.string.inception));
        moviesList.add(new Movies("Avengers Infinity War",R.drawable.avengers_infinity_war_poster,R.drawable.avengers_infinitywar_bg,R.string.avengers_infinity_war));
        moviesList.add(new Movies("Interstellar",R.drawable.interstellar_film_poster,R.drawable.interstellar_bg,R.string.interstellar));
        moviesList.add(new Movies("Dark Knight",R.drawable.dark_knight,R.drawable.dark_knight_bg,R.string.dark_knight));
        moviesList.add(new Movies("Terminator 2",R.drawable.terminator,R.drawable.terminator_bg,R.string.terminator_2));
        moviesList.add(new Movies("Expendables 2",R.drawable.expendables_2_poster,R.drawable.expendables_2_bg,R.string.expendables_2));
        return moviesList;
    }
    public static List<Movies> getWeekMovies(){
        List<Movies> moviesList = new ArrayList<>();
        moviesList.add(new Movies("Avatar", R.drawable.avtar_poster,R.drawable.avtar_bg,R.string.avatar));
        moviesList.add(new Movies("Expendables",R.drawable.expendables_1_poster,R.drawable.expendables_1_bg,R.string.expendables));
        moviesList.add(new Movies("Jurassic Park",R.drawable.jurassic_park_poster,R.drawable.jurassic_park_bg,R.string.jurassic_park));
        moviesList.add(new Movies("Mechanic",R.drawable.mechanic_poster,R.drawable.mechanic_bg,R.string.mechanic));
        moviesList.add(new Movies("Expendables 3",R.drawable.expendables_3_poster,R.drawable.expendables_3_bg,R.string.expendables_3));
        moviesList.add(new Movies("Mission Impossible Fallout",R.drawable.mission_impossible_fallout_poster,R.drawable.mission_impossible_fallout_bg,R.string.mission_impossible_fallout));
        moviesList.add(new Movies("Pirates Of The Caribbean",R.drawable.pirates_of_the_caribbean_poster,R.drawable.pirates_of_the_caribbean_bg,R.string.pirates_of_the_caribbean));
        return moviesList;
    }
    public static List<Movies> getUpComingMovies(){
        List<Movies> moviesList = new ArrayList<>();
        moviesList.add(new Movies("Gladiator", R.drawable.gladiator_poster,R.drawable.gladiator_bg,R.string.gladiator));
        moviesList.add(new Movies("Mechanic Resurrection",R.drawable.mechanic_resurrection_poster,R.drawable.mechanic_resurrection_bg,R.string.mechanic_resurrection));
        moviesList.add(new Movies("Rambo First Blood",R.drawable.rambo_1_poster,R.drawable.rambo_1_bg,R.string.ramb_first_blood));
        moviesList.add(new Movies("Rambo 2",R.drawable.rambo_2_poster,R.drawable.rambo_2_bg,R.string.rambo_2));
        moviesList.add(new Movies("Rambo 3",R.drawable.rambo_3_poster,R.drawable.rambo_3_bg,R.string.rambo_3));
        moviesList.add(new Movies("The Mummy",R.drawable.the_mummy_poster,R.drawable.the_mummy_bg,R.string.the_mummy));
        moviesList.add(new Movies("Titanic",R.drawable.titanic_poster,R.drawable.titanic_bg,R.string.titanic));
        return moviesList;
    }
    public static List<Cast> getAvengersEndGameCast(){
        List<Cast> castList = new ArrayList<>();
        castList.add(new Cast("Robert Downey Jr",R.drawable.robert_downey_jr));
        castList.add(new Cast("Chris Hemsworth",R.drawable.chris_hemsworth));
        castList.add(new Cast("Chris Evans",R.drawable.chris_evans));
        castList.add(new Cast("Scarlett Johansson",R.drawable.scarlett_johansson));
        castList.add(new Cast("Mark Ruffalo",R.drawable.mark_ruffalo));
        castList.add(new Cast("Don Cheadle",R.drawable.don_cheadle));
        castList.add(new Cast("Benedict Cumberbatch",R.drawable.benedict_cumberbatch));
        return castList;

    }
}

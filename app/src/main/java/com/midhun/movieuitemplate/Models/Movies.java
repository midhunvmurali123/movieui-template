package com.midhun.movieuitemplate.Models;

import java.util.List;

public class Movies  {


    private String title;
    private int description;
    private int thumbainail;
    private String studio;
    private String rating;
    private String streamingLink;
    private int coverImage;

    public Movies(String title, int thumbainail, int coverImage, int description, Cast castList) {
        this.title = title;
        this.description = description;
        this.thumbainail = thumbainail;
        this.coverImage = coverImage;
    }

    public Movies(String title, int thumbainail, int coverImage, int description) {
        this.title = title;
        this.thumbainail = thumbainail;
        this.coverImage = coverImage;
        this.description = description;
    }

    public Movies(String title, int avengers_endgame_poster, int avengers_endgame_bg, int thumbainail, List<Cast> avengersEndGameCast) {
        this.title = title;
        this.thumbainail = thumbainail;
    }

    public Movies(String title, int description, int thumbainail, String studio, String rating, String streamingLink) {
        this.title = title;
        this.description = description;
        this.thumbainail = thumbainail;
        this.studio = studio;
        this.rating = rating;
        this.streamingLink = streamingLink;
    }

    public int getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(int coverImage) {
        this.coverImage = coverImage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getDescription() {
        return description;
    }

    public void setDescription(int description) {
        this.description = description;
    }

    public int getThumbainail() {
        return thumbainail;
    }

    public void setThumbainail(int thumbainail) {
        this.thumbainail = thumbainail;
    }

    public String getStudio() {
        return studio;
    }

    public void setStudio(String studio) {
        this.studio = studio;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getStreamingLink() {
        return streamingLink;
    }

    public void setStreamingLink(String streamingLink) {
        this.streamingLink = streamingLink;
    }


}

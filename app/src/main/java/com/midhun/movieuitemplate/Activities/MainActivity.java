package com.midhun.movieuitemplate.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.midhun.movieuitemplate.Adapters.MovieAdapter;
import com.midhun.movieuitemplate.Utils.DataSource;
import com.midhun.movieuitemplate.Utils.MovieItemClickListener;
import com.midhun.movieuitemplate.Adapters.SliderPagerAdapter;
import com.midhun.movieuitemplate.Models.Movies;
import com.midhun.movieuitemplate.Models.Slide;
import com.midhun.movieuitemplate.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements MovieItemClickListener {
    private List<Slide> slideList;
    private ViewPager slidePager;
    private SliderPagerAdapter sliderPagerAdapter;
    private TabLayout indicator;
    private RecyclerView movieRecyclerView,rvMovieWeek,rvUpComing;
    private MovieAdapter movieAdapter,movieWeekAdapter,upComingAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initViews();
        initSlider();
        initPopularMovies();
        initWeekMovies();
        initUpComingMovies();


    }

    private void initUpComingMovies() {
        upComingAdapter = new MovieAdapter(this, DataSource.getUpComingMovies(),this);
        rvUpComing.setAdapter(upComingAdapter);
        rvUpComing.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,false));
    }

    private void initWeekMovies() {
        movieWeekAdapter = new MovieAdapter(this, DataSource.getWeekMovies(),this);
        rvMovieWeek.setAdapter(movieWeekAdapter);
        rvMovieWeek.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,false));
    }

    private void initPopularMovies() {
        //Recyclerview Setup


        movieAdapter = new MovieAdapter(this, DataSource.getPopularMovies(),this);
        movieRecyclerView.setAdapter(movieAdapter);
        movieRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,false));
    }

    private void initSlider() {
        slideList = new ArrayList<>();
        sliderPagerAdapter = new SliderPagerAdapter(this,slideList);

        //List of items
        slideList.add(new Slide(R.drawable.slide1,"Avengers"));
        slideList.add(new Slide(R.drawable.slide2,"Harry Porter"));
        slideList.add(new Slide(R.drawable.slide3,"Pirates of the Caribbean"));
        slideList.add(new Slide(R.drawable.slide4,"Spider Man"));
        slideList.add(new Slide(R.drawable.slide5,"Transformers"));
        slideList.add(new Slide(R.drawable.slide6,"The Wolverine"));

        slidePager.setAdapter(sliderPagerAdapter);
        // Setup timer
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new SliderTimer(),4000,4000);
        indicator.setupWithViewPager(slidePager,true);
    }

    private void initViews() {
        slidePager = findViewById(R.id.slider_pager);
        indicator = findViewById(R.id.indicator);
        movieRecyclerView = findViewById(R.id.rv_movies);
        rvMovieWeek = findViewById(R.id.rv_movies_week);
        rvUpComing = findViewById(R.id.rv_movies_upcoming);
    }

    @Override
    public void onMovieClick(Movies movies, ImageView movieImageView) {
        //send movie information to details activity
        //create transition animation between two activity
        Intent intent = new Intent( MainActivity.this, MovieDetailsActivity.class);
        Movies movies1 = (Movies) movies;
        intent.putExtra("title",movies.getTitle());
        intent.putExtra("imgURL",movies.getThumbainail());
        intent.putExtra("imgCover",movies.getCoverImage());
        intent.putExtra("description",movies.getDescription());


        startActivity(intent);
        //animation
        ActivityOptions options = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            options = ActivityOptions.makeSceneTransitionAnimation(MainActivity.this,movieImageView,"sharedName");
        }
        startActivity(intent,options.toBundle());

        Toast.makeText(this,"Item Clicked:"+movies.getTitle(),Toast.LENGTH_LONG).show();
    }

    class SliderTimer extends TimerTask {
        @Override
        public void run() {
            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(slidePager.getCurrentItem()<slideList.size()-1){
                        slidePager.setCurrentItem(slidePager.getCurrentItem()+1);
                    }
                    else {
                        slidePager.setCurrentItem(0);
                    }
                }
            });
        }
    }
}

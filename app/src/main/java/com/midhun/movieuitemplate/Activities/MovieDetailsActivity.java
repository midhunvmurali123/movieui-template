package com.midhun.movieuitemplate.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.codesgood.views.JustifiedTextView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.midhun.movieuitemplate.Adapters.CastAdapter;

import com.midhun.movieuitemplate.R;
import com.midhun.movieuitemplate.Utils.DataSource;

import java.util.ArrayList;


public class MovieDetailsActivity extends AppCompatActivity {
    private ImageView MovieThumbnailImg,MovieCoverImg;
    private TextView movieTitle;
    private JustifiedTextView movieDescription;
    private FloatingActionButton play_fab;
    private RecyclerView castView;
    private CastAdapter castAdapter;
    ArrayList<String> castList;
    String movTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);
        MovieThumbnailImg = findViewById(R.id.details_movie_image);
        MovieCoverImg = findViewById(R.id.deatils_movie_cover);
        movieTitle = findViewById(R.id.details_movie_title);
        movieDescription = findViewById(R.id.details_movie_description);
        play_fab = findViewById(R.id.play_fab);
        castView = findViewById(R.id.rv_cast);
        castList = new ArrayList<>();


        initViews();

        setUpRvCast();

    }



    private void initViews() {
        //get data
        movTitle = getIntent().getExtras().getString("title");
        int description = getIntent().getExtras().getInt("description");
        int imageResourceId = getIntent().getExtras().getInt("imgURL");
        int coverResourceId = getIntent().getExtras().getInt("imgCover");
        castList = getIntent().getStringArrayListExtra("test");
        MovieThumbnailImg.setImageResource(imageResourceId);
        movieTitle.setText(movTitle);
        movieDescription.setText(description);
        getSupportActionBar().setTitle(movTitle);
        Glide.with(this).applyDefaultRequestOptions(new RequestOptions().disallowHardwareConfig()).load(imageResourceId).into(MovieThumbnailImg);
        Glide.with(this).applyDefaultRequestOptions(new RequestOptions().disallowHardwareConfig()).load(coverResourceId).into(MovieCoverImg);
        //set up animation
        MovieCoverImg.setAnimation(AnimationUtils.loadAnimation(this,R.anim.scale_animation));
        play_fab.setAnimation(AnimationUtils.loadAnimation(this,R.anim.scale_animation));
    }
    private void setUpRvCast() {

        switch (movTitle) {
            case"Avengers Endgame":
                castAdapter = new CastAdapter(this, DataSource.getAvengersEndGameCast());
                break;
        }




        castView.setAdapter(castAdapter);
        castView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,false));

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent home = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(home);
        finish();
    }
}
